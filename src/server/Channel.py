class Channel:
    def __init__(self, path):
        self.path = path
        self.clients = set()

    def addClient(self, client):
        self.clients.add(client)

    def removeClient(self, client):
        self.clients.remove(client)
