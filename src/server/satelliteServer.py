import asyncio
import websockets
import random
import environs

from satellite_network_protocol.src.FlagsMask import FlagsMask
from satellite_network_protocol.src.package import Package
import satellite_network_protocol.src.utilities as snpUtils
from server.Client import Client
from server.Channel import Channel


class Satellite():

    def __init__(self, logger):
        self.usedIDs = set()
        self.channels = set()
        self.logger = logger


    async def getNewID(self):
        newID = random.randint(1, 65534)
        while newID in self.usedIDs:
            newID = random.randint(1, 65534)

        self.usedIDs.add(newID)
        return newID


    async def broadcast(self, sender, msg, channel):
        clients = [cl.id for cl in channel.clients if cl != sender]
        if clients:  # asyncio.wait doesn't accept an empty list
            await asyncio.wait(
                    [cl.socket.send(msg) for cl in channel.clients if cl != sender])


    async def singlecast(self, sender, msg, recvID, channel):
        for user in channel.clients:
            if user.id == recvID:
                await user.socket.send(msg)


    async def register(self, websocket, channel):
        clientID = await self.getNewID()
        client = Client(websocket, clientID)
        channel.addClient(client)

        self.logger.info(f'Client connected:    {clientID}')
        return client


    def unregister(self, client, channel):
        oldID = client.id
        self.usedIDs.remove(client.id)

        channel.removeClient(client)
        del(client)

        self.logger.info(f'Client disconnected: {oldID}')


    async def relayMessages(self, websocket, path):
        # Get login package from websocket
        msg = await websocket.recv()
        pkg = Package(msg)
        if not pkg.isControl or not pkg.payload[0] == 2:
            errorMessage = 'First message needs to be conn. req.!'
            await websocket.send(snpUtils.buildCtrlConnRefused(errorMessage))
            self.logger.info('Refused connection. Unknown client sent \
                    incorrect initial package.')
            return

        # Check if token exists
        receivedToken = pkg.payload[1:].decode('utf-8')
        channel = None
        for c in self.channels:
            if c.path == receivedToken:
                # If path existes, register client
                channel = c
                client = await self.register(websocket, channel)
                break

        # If path does not exist, deny connection
        if channel is None:
            errorMessage = 'Incorrect token'
            await websocket.send(snpUtils.buildCtrlConnRefused(errorMessage))
            self.logger.info('Refused connection. Unknown client sent \
                    incorrect token.')
            return

        try:
            # Send client his ID -> confirm connection req. was successfull
            await client.socket.send(snpUtils.buildCtrlClientID(client.id))

            # Relay packages
            async for message in websocket:
                pkg = Package(message)

                # If sender ID is not correct, relay package with corrected ID
                # but let the client know his correct ID
                if pkg.senderID != client.id:
                    msg = snpUtils.buildCtrlClientID(client.id)
                    await client.socket.send(msg)
                    pkg.senderID = client.id
                    message = pkg.toByteMessage()

                # Take the received package and relay it to either the whole
                # channel or to a specified target.
                if pkg.recvID == 65535:
                    await self.broadcast(client, message, channel)
                else:
                    await self.singlecast(client, message, pkg.recvID, channel)

        except websockets.exceptions.ConnectionClosed:
            # An error occured in the process. This is no issue.
            self.logger.debug('Connection closed while sending!')
        finally:
            # No matter if disconnected gracefully or with an error, the client
            # is unregistered from his channel
            self.unregister(client, channel)
