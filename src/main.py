import asyncio
import websockets
import logging
import environs
import sys

import server.satelliteServer as satServer
from server.Channel import Channel


def startServer():
    # Configure logging
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)

    logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)s: %(message)s',
            handlers=[handler])

    logger = logging.getLogger(__name__)

    # Create server
    satellite = satServer.Satellite(logger)

    # Create channels
    env = environs.Env()
    env.read_env()
    paths = env.list('CHANNEL_TOKENS', subcast=str, delimiter=',')
    for path in paths:
        satellite.channels.add(Channel(path))

    # Get port
    try:
        PORT = env('PORT')
    except environs.EnvError:
        logger.warning('No "PORT" environment variabel found. Defaulting to port 8765.')
        PORT = 8765

    # Create service
    start_server = websockets.serve(satellite.relayMessages, "localhost", PORT)

    # Start server and run indefinitely
    logger.info(f'Server started. Listening on port {PORT}')
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()

if __name__ == '__main__':
    startServer()
