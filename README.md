# Satellite Server

## About

The Satellite Server is at it's core a proxy server. Clients can connect to it and subscribe to channels over which they can send and receive messages to and from all other clients in that channel. In addition the server also manages the clients IDs and coordinates the intial connection.


## Setup

1. Clone the repository and it's submodules with either SSH:
    ```sh
    git clone --recurse-submodules git@gitlab.com:remote_exploration_simulation/satellite_server.git
    ```
    Or HTTPs:
    ```sh
    git clone --recurse-submodules https://gitlab.com/remote_exploration_simulation/satellite_server.git`
    ```

2. Enter repository directory. Then initialize a virtual environment and enter it:
    ```sh
    cd satellite_server
    python3 -m venv venv
    source venv/bin/activate
    ```

3. Install required packages
    ```sh
    pip3 install -r requirements.txt
    ```

4. Create `.env` file to store your port number and your channel tokens. 
The port can be any port you like. 
Each of the channel tokens will create one channel on the server on startup. The clients access the channel by that token. Tokes can be made arbitrarily complex to only allow clients in that know one of the tokes. BUT make sure, that the tokens are shorter than 123 bytes!:
    ```sh
    touch .env
    echo "PORT = <Your port number>" >> .env
    echo "CHANNEL_TOKENS = <Your comma separated tokens>" >> .env
    ```


## Starting the server

In order to start the server simply run
```sh
python3 src/main.py
```


## Stopping the server

The server runs inefinitely and can, for now, only be forcefully stopped by pressing `Ctrl`+`C`.
